import "./blogList.css";
import React, { useState, useEffect } from "react";
import BlogListItem from "../blogListItem/blogListItem";
import { db } from "../firebase";
import { collection, getDocs } from "@firebase/firestore";

export default function BlogList() {
  const [posts, setPosts] = useState([]);
  const postsCollectionRef = collection(db, "posts");

  useEffect(() => {
    getPosts();
  }, []);

  function getPosts() {
    getDocs(postsCollectionRef).then((res) =>
      setPosts(res.docs.map((doc) => ({ ...doc.data(), id: doc.id })))
    );
  }

  return (
    <div className="container">
      <div className="d-grid blog-list">
        {posts?.map((item) => (
          <BlogListItem key={item.id} item={item} id={item.id} />
        ))}
      </div>
    </div>
  );
}
