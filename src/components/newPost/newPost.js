import React, { useState } from "react";
import styles from "./newPost.module.scss";
import { db } from "../firebase";
import { collection, addDoc } from "@firebase/firestore";
export default function NewPost() {
  const postsCollectionRef = collection(db, "posts");
  const [newTitle, setNewTitle] = useState("");
  const [newText, setNewText] = useState("");
  const [newDate, setNewDate] = useState("");
  const [newView, setNewView] = useState("");
  const [newImgL, setNewImgL] = useState("");

  const createNewPost = async (e) => {
    e.preventDefault();
    await addDoc(postsCollectionRef, {
      title: newTitle,
      text: newText,
      date: newDate,
      imgl: newImgL,
      imgb: newImgL,
      view: newView,
    });
    console.log(postsCollectionRef);
  };

  return (
    <div className={styles.newPost}>
      <div className={styles.container}>
        <form className={styles.form} action="">
          <input
            type="text"
            placeholder="Write title"
            onChange={(e) => setNewTitle(e.target.value)}
          />
          <input
            type="text"
            placeholder="Write date"
            onChange={(e) => setNewDate(e.target.value)}
          />
          <input
            type="text"
            placeholder="Write view"
            onChange={(e) => setNewView(e.target.value)}
          />
          <input
            type="text"
            placeholder="Write img url"
            onChange={(e) => setNewImgL(e.target.value)}
          />
          <textarea
            type="text"
            placeholder="Write text"
            onChange={(e) => setNewText(e.target.value)}
          />
          <button className={styles.newPostBtn} onClick={createNewPost}>
            Создать
          </button>
        </form>
      </div>
    </div>
  );
}
