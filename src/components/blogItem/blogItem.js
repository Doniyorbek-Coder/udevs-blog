import React, { useEffect, useState } from "react";
import "./blogItem.css";
import BlogItemContent from "./blogItemContent";
import { useParams } from "react-router-dom";
import { db } from "../firebase";
import { collection, getDocs } from "@firebase/firestore";

export default function BlogItem() {
  const [posts, setPosts] = useState([]);
  const { id } = useParams();
  const postsCollectionRef = collection(db, "posts");

  useEffect(() => {
    getPosts();
  }, []);

  function getPosts() {
    getDocs(postsCollectionRef).then((res) =>
      setPosts(res.docs.map((doc) => ({ ...doc.data(), id: doc.id })))
    );
  }

  return (
    <div>
      {posts?.map((post) => {
        return id === post.id && <BlogItemContent key={post.id} post={post} />;
      })}
    </div>
  );
}
