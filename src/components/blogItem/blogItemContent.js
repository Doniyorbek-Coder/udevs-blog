import React from "react";
import userImg from "../images/userImg.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas, faBookmark, faEye } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

export default function BlogItemContent({ post }) {
  return (
    <div className="container blog-item__container" key={post.id}>
      <div className="blog-item__user">
        <Link to={"/profile"} className="user-content">
          <img className="user-image" src={userImg} alt="user" />
          <span className="user-name">Dilorom Aliyeva</span>
        </Link>
        <div className="user-buttons">
          <button className="btn btn-follow">Follow</button>
          <button className="btn btn-saved">
            <FontAwesomeIcon icon={(fas, faBookmark)} />
          </button>
        </div>
      </div>
      <div className="blog-item__content">
        <img src={post.imgb} alt="card" className="blog-item__img" />
        <span className="image-owner">
          Фото: <i>Dilorom Alieva</i>
        </span>
        <div className="card-info">
          <p className="card-date">{post.date}</p>
          <FontAwesomeIcon icon={(fas, faEye)} />
          <span className="card-number">{post.view}</span>
        </div>
        <h2 className="blog-item__title">{post.title}</h2>
        <p className="blog-item__text">{post.text}</p>
      </div>
      <div className="blog-item__famous">
        <h3 className="blog-item__title">ЛУЧШИЕ БЛОГИ</h3>
        <div to={`/blogs`} className="card">
          <img
            className="card-img-top"
            src={post.imgl}
            alt="card img"
            style={{ width: "125px", height: "115px" }}
          />
          <div className="card-body">
            <h4 className="card-title">{post.title}</h4>
            <div className="card-info">
              <p className="card-date">{post.date}</p>
              <FontAwesomeIcon icon={(fas, faEye)} />
              <span className="card-number">{post.view}</span>
            </div>
          </div>
        </div>
        <div to={`/blogs`} className="card">
          <img
            className="card-img-top"
            src={post.imgl}
            alt="card img"
            style={{ width: "125px", height: "115px" }}
          />
          <div className="card-body">
            <h4 className="card-title">{post.title}</h4>
            <div className="card-info">
              <p className="card-date">{post.date}</p>
              <FontAwesomeIcon icon={(fas, faEye)} />
              <span className="card-number">{post.view}</span>
            </div>
          </div>
        </div>
        <div to={`/blogs`} className="card">
          <img
            className="card-img-top"
            src={post.imgl}
            alt="card img"
            style={{ width: "125px", height: "115px" }}
          />
          <div className="card-body">
            <h4 className="card-title">{post.title}</h4>
            <div className="card-info">
              <p className="card-date">{post.date}</p>
              <FontAwesomeIcon icon={(fas, faEye)} />
              <span className="card-number">{post.view}</span>
            </div>
          </div>
        </div>
        <div to={`/blogs`} className="card">
          <img
            className="card-img-top"
            src={post.imgl}
            alt="card img"
            style={{ width: "125px", height: "115px" }}
          />
          <div className="card-body">
            <h4 className="card-title">{post.title}</h4>
            <div className="card-info">
              <p className="card-date">{post.date}</p>
              <FontAwesomeIcon icon={(fas, faEye)} />
              <span className="card-number">{post.view}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
