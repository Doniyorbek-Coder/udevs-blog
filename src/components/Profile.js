import React from "react";
import Footer from "./footer/footer";
import Header from "./header/header";
import ProfileBlog from "./profileBlog/ProfileBlog";

export default function Profile() {
  return (
    <div>
      <Header />
      <ProfileBlog />
      <Footer />
    </div>
  );
}
