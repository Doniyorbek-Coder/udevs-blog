import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./header.css";
import userImg from "../images/userImg.png";
import logo from "../images/logo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas, faBell, faTimes } from "@fortawesome/free-solid-svg-icons";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import { auth } from "../firebase";

export default function Header() {
  const [headerColor, setHeaderColor] = useState("100px");
  const [index, setIndex] = useState("absolute");
  const [isOpen, setIsOpen] = useState(false);
  const [isSignOpen, setIsSignOpen] = useState(false);
  const [isLogged, setIsLogged] = useState(false);
  const [user, setUser] = useState({});
  const [profile, setProfile] = useState(false);
  const [signEmail, setSignEmail] = useState("");
  const [signPassword, setSignPassword] = useState("");
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const listenScrollEvent = () => {
    window.scrollY < 75
      ? setHeaderColor(`${100 - window.scrollY}px`)
      : setHeaderColor("25px");
    window.scrollY > 75 ? setIndex("fixed") : setIndex("absolute");
  };
  window.addEventListener("scroll", listenScrollEvent);

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  async function SignUser() {
    try {
      const user = await createUserWithEmailAndPassword(
        auth,
        signEmail,
        signPassword
      );
      setIsSignOpen(false);
    } catch (error) {
      alert(error.message);
    }
  }

  async function loginUser() {
    try {
      const user = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      );
      closeBtn();
      setIsLogged(true);
    } catch (error) {
      alert(error.message);
    }
  }
  async function logoutHandler() {
    await signOut(auth);
  }
  function loginViaReg() {
    setIsSignOpen(false);
    loginModal();
  }
  function loginModal() {
    setIsOpen(true);
  }
  function SignModal() {
    setIsSignOpen(true);
  }

  function closeBtn() {
    setIsOpen(false);
  }
  function closeSign() {
    setIsSignOpen(false);
  }
  function onSubmit(e) {
    e.preventDefault();
  }

  return (
    <header className="home-header">
      <div className="header-top">
        <div className="container header-top__container">
          <Link className="site-logo" to={"/"}>
            <img
              src={logo}
              style={{ width: "146px", height: "49px" }}
              alt="site-logo"
            />
          </Link>
          <nav
            className="header__nav"
            style={{ top: `${headerColor}`, position: `${index}` }}
          >
            <div className="container header-nav__container">
              <ul className="nav nav-pills">
                <li className="nav-item">
                  <a className="nav-link active" href="/">
                    Все потоки
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    Разработка
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    Администрирование
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    Дизайн
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    Менеджмент
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    Маркетинг
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    Научпоп
                  </a>
                </li>
              </ul>
            </div>
          </nav>
          <div className="header-top__notification">
            <FontAwesomeIcon
              className="notification__bell"
              icon={(fas, faBell)}
            />
            <span className="notification__count">1</span>
          </div>
          {user && !isLogged && (
            <button onClick={loginModal} className="nav-link register">
              Log In
            </button>
          )}
          {user && isLogged && (
            <div className="profile">
              <button className="profile-user" onClick={() => setProfile(true)}>
                <img className="profile-img" src={userImg} alt="user img" />
              </button>
              <ul className={`profile-menu ${profile ? "visible" : ""}`}>
                <li className="profile-item">
                  <div className="arrow-up"></div>
                  <Link to={"/create"} onClick={() => setIsLogged(true)}>
                    Написать публикацию
                  </Link>
                </li>
                <li className="profile-item">
                  <Link to={"/"}>Избранные</Link>
                </li>
                <li className="profile-item">
                  <button onClick={logoutHandler} className="profile-btn">
                    Выйти
                  </button>
                </li>
              </ul>
            </div>
          )}
          {!user && (
            <button onClick={SignModal} className="nav-link register">
              Sign In
            </button>
          )}
        </div>
      </div>
      <div className={`auth ${isOpen ? "visible" : ""}`}>
        <div className="auth__container">
          <button onClick={closeBtn} className="auth__close">
            <FontAwesomeIcon icon={faTimes} />
          </button>
          <h2 className="auth__title">Вход на udevs news</h2>
          <form className="auth__form" onSubmit={onSubmit}>
            <input
              className="form-control"
              type="email"
              placeholder="Email"
              onChange={(e) => setLoginEmail(e.target.value)}
            />
            <input
              className="form-control"
              type="password"
              placeholder="Пароль"
              onChange={(e) => setLoginPassword(e.target.value)}
            />
            <button onClick={loginUser} className="btn auth__btn" type="submit">
              Войти
            </button>
          </form>
        </div>
      </div>
      <div className={`auth ${isSignOpen ? "visible" : ""}`}>
        <div className="auth__container">
          <button onClick={closeSign} className="auth__close">
            <FontAwesomeIcon icon={faTimes} />
          </button>
          <h2 className="auth__title">Reg на udevs news</h2>
          <form className="auth__form" onSubmit={onSubmit}>
            <input
              className="form-control"
              type="email"
              placeholder="Email"
              onChange={(e) => setSignEmail(e.target.value)}
            />
            <input
              className="form-control"
              type="password"
              placeholder="Пароль"
              onChange={(e) => setSignPassword(e.target.value)}
            />
            <button onClick={SignUser} className="btn auth__btn mb-2">
              Reg
            </button>

            <button
              onClick={loginViaReg}
              className="btn auth__btn"
              type="submit"
            >
              Войти
            </button>
          </form>
        </div>
      </div>
    </header>
  );
}
