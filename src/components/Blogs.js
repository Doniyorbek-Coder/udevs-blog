import React from "react";
import Footer from "./footer/footer";
import BlogItem from "./blogItem/blogItem";
import Header from "./header/header";

export default function Blogs() {
  return (
    <div>
      <Header />
      <BlogItem />
      <Footer />
    </div>
  );
}
