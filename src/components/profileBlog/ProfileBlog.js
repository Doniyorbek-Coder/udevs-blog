import React, { useState, useEffect } from "react";
import userImg from "../images/userImg.png";
import styles from "../styles/profile.module.scss";
import ProfileBlogItem from "./ProfileBlogItem";
import { db } from "../firebase";
import { collection, getDocs } from "@firebase/firestore";

export default function ProfileBlog() {
  const [posts, setPosts] = useState([]);
  const postsCollectionRef = collection(db, "posts");

  useEffect(() => {
    getPosts();
  }, []);

  function getPosts() {
    getDocs(postsCollectionRef).then((res) =>
      setPosts(res.docs.map((doc) => ({ ...doc.data(), id: doc.id })))
    );
  }

  return (
    <div className={styles.profile}>
      <div className={styles.container}>
        <div className={styles.avatar}>
          <img className={styles.avatarImg} src={userImg} alt="user img" />
          <div>
            <h2 className={styles.avatarTitle}>Дилором Алиева</h2>
            <table>
              <tr>
                <td>Карьера</td>
                <td>Писатель</td>
              </tr>
              <tr>
                <td>Дата рождения</td>
                <td>2 ноября, 1974 ( 46 лет)</td>
              </tr>
              <tr>
                <td>Место рождения</td>
                <td>Черняховск, СССР (Россия)</td>
              </tr>
            </table>
          </div>
        </div>
        <div className={styles.profileNews}>
          <h3>ПУБЛИКАЦИИ</h3>
          <ul className={styles.profileList}>
            {posts?.map((item) => (
              <ProfileBlogItem key={item.id} item={item} id={item.id} />
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}
