import React from "react";
import styles from "../styles/profile.module.scss";
export default function ProfileBlogItem({ item }) {
  return (
    <div className={styles.profileItem}>
      <img className={styles.profileImg} src={item.imgb} alt="profileBlog" />
      <div>
        <h3>{item.title}</h3>
        <div className={styles.profileContent}>
          <p className={styles.profileDate}>{item.date}</p>
          <a href="#">Права человека</a>
        </div>
        <p>{item.text}</p>
        <button className={styles.profileBnt}>Читать</button>
      </div>
    </div>
  );
}
