import React from "react";
import "./blogListItem.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas, faEye } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

export default function BlogListItem({ item, id }) {
  return (
    <Link to={`/blogs/${id}`} className="card">
      <img
        className="card-img-top"
        src={item.imgl}
        alt="card img"
        style={{ width: "100%", height: "300px" }}
      />
      <div className="card-body">
        <div className="card-info">
          <p className="card-date">{item.date}</p>
          <FontAwesomeIcon icon={(fas, faEye)} />
          <span className="card-number">{item.view}</span>
        </div>
        <h4 className="card-title">{item.title}</h4>
      </div>
    </Link>
  );
}
