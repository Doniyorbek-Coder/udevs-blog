import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBt12JrmcjsoH4gjI5UFsVg2D6fyD21BTk",
  authDomain: "udevs-bolg-a.firebaseapp.com",
  projectId: "udevs-bolg-a",
  storageBucket: "udevs-bolg-a.appspot.com",
  messagingSenderId: "693624175766",
  appId: "1:693624175766:web:bad0a025ed21fd6d53d6b7",
  measurementId: "${config.measurementId}",
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app);
