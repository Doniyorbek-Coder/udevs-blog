import React from "react";
import BlogList from "./blogList/blogList";
import Header from "./header/header";
import Footer from "./footer/footer";

export default function Home() {
  return (
    <div>
      <Header />
      <BlogList />
      <Footer />
    </div>
  );
}
