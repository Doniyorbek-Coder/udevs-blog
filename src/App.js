import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import Blogs from "./components/Blogs";
import CreatePost from "./components/createPost";
import Profile from "./components/Profile";

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path={"/blogs/:id"} element={<Blogs />} />
          <Route path={"/create"} element={<CreatePost />} />
          <Route path={"/profile"} element={<Profile />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
